import moment from 'moment';
import joi from '@hapi/joi';
import 'joi-extract-type';
import { injectable } from 'inversify';

import { CommandType } from '../types/enums';
import { StateService } from '../types/state';

interface CommandTypeState {
    quantity: number;
    totalPrice: number;
}

type DailyState = {
    [type in Exclude<CommandType, CommandType.message>]: CommandTypeState;
};

function todayString(): string {
    return moment().format('YYYY-MM-DD');
}

function emptyStateForType(): CommandTypeState {
    return { quantity: 0, totalPrice: 0 };
}

function emptyStateForDay(): DailyState {
    return {
        [CommandType.tea]: emptyStateForType(),
        [CommandType.hotChocolate]: emptyStateForType(),
        [CommandType.coffee]: emptyStateForType(),
        [CommandType.orangeJuice]: emptyStateForType(),
        [CommandType.extraHotTea]: emptyStateForType(),
        [CommandType.extraHotChocolate]: emptyStateForType(),
        [CommandType.extraHotCoffee]: emptyStateForType(),
    };
}

@injectable()
export class CoffeeMachineState implements StateService {

    // Should be persistent
    private dailyState: { [date: string]: DailyState } = {};

    private stateForDay(dateString: string): DailyState {
        if (!this.dailyState[dateString]) {
            this.dailyState[dateString] = emptyStateForDay();
        }
        return this.dailyState[dateString];
    }

    private total(dateString: string): number {
        const state = this.stateForDay(dateString);
        return Object.keys(state).reduce((res, type) => {
            return res + (state[type as Exclude<CommandType, CommandType.message>]?.totalPrice ?? 0);
        }, 0);
    }

    public report(date: string): string {
        const { error, value: validatedDate } = joi.date().validate(date);
        if (error) {
            throw new Error(`Error: Invalid date "${date}"`);
        }
        const dateString = moment(validatedDate).format('YYYY-MM-DD');
        const state = this.stateForDay(dateString);
        return [
            ' === Quantities === ',
            `  Tea: ${state[CommandType.tea].quantity}`,
            `  Hot chocolate: ${state[CommandType.hotChocolate].quantity}`,
            `  Coffee: ${state[CommandType.coffee].quantity}`,
            `  Orange juice: ${state[CommandType.orangeJuice].quantity}`,
            `  Extra Hot Tea: ${state[CommandType.extraHotTea].quantity}`,
            `  Extra Hot chocolate: ${state[CommandType.extraHotChocolate].quantity}`,
            `  Extra Hot Coffee: ${state[CommandType.extraHotCoffee].quantity}`,
            ' === Earnings === ',
            `  total: ${this.total(dateString).toFixed(2)} euros`,
        ].join('\n');
    }

    public sold(type: Exclude<CommandType, CommandType.message>, price: number): void {
        const dateString = todayString();
        const state = this.stateForDay(dateString);
        state[type].quantity += 1;
        state[type].totalPrice += price;
    }

}