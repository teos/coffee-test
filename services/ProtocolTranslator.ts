import joi from '@hapi/joi';
import 'joi-extract-type';
import { injectable, inject } from 'inversify';

import { preciseRound } from '../helpers/math';
import { CommandType } from '../types/enums';
import { ProtocolData } from '../types/protocol';
import { StateService } from '../types/state';
import { ProtocolTranslatorService, OrderData } from '../types/coffee';
import { BeverageQuantityCheckerInterface, EmailNotifierInterface } from '../types/external';

export const orderPrices: {
    [type in Exclude<CommandType, CommandType.message>]: number;
} = {
    [CommandType.tea]: 0.4,
    [CommandType.hotChocolate]: 0.5,
    [CommandType.coffee]: 0.6,
    [CommandType.orangeJuice]: 0.6,
    [CommandType.extraHotTea]: 0.4,
    [CommandType.extraHotChocolate]: 0.5,
    [CommandType.extraHotCoffee]: 0.6,
};

export const OrderInputSchema = joi.object({
    command: joi.string().valid(...Object.values(CommandType)).required(),
    sugar: joi.number().optional().integer().min(0).default(0),
    cash: joi.number().optional().min(0).default(0),
    stick: joi.boolean(),
    message: joi.string().allow(''),
}).options({ stripUnknown: true });

@injectable()
export class ProtocolTranslator implements ProtocolTranslatorService {

    constructor(
        @inject('BeverageQuantityCheckerInterface') private beverageQuantityChecker: BeverageQuantityCheckerInterface,
        @inject('EmailNotifierInterface') private emailNotifier: EmailNotifierInterface,
        @inject('StateService') private stateService: StateService,
    ) {}

    public translate(data: ProtocolData): string {
        const result: string[] = [ data.command ];
        if (data.command === CommandType.message) {
            result.push(data.message);
        } else {
            if (data.sugar > 0) {
                result.push(String(data.sugar));
            } else {
                result.push('');
            }
            result.push(data.stick ? '0' : '');
        }
        return result.join(':');
    }
    
    public checkCash(command: Exclude<CommandType, CommandType.message>, cash: number): number {
        return preciseRound(Math.min(orderPrices[command], (orderPrices[command] - cash), 2));
    }
    
    public order(data: OrderData): string {
        const { error, value } = OrderInputSchema.validate(data);
        if (error) {
            // console.error(error);
            return this.translate({ command: CommandType.message, message: 'Invalid input' });
        }
        const order = value as unknown as joi.extractType<typeof OrderInputSchema>;
        if (order.command === CommandType.message) {
            return this.translate({
                command: order.command,
                message: order.message || '',
            });
        }
        const missingCash = this.checkCash(order.command, order.cash);
        if (missingCash > 0) {
            return this.translate({
                command: CommandType.message,
                message: `Missing ${missingCash} euros`,
            });
        }
        if (!this.beverageQuantityChecker.isEmpty(order.command)) {
            this.emailNotifier.notifyMissingDrink(order.command);
            return this.translate({
                command: CommandType.message,
                message: 'Shortage, the service company has been notified',
            });
        }
        this.stateService.sold(order.command, orderPrices[order.command]);
        return this.translate({
            command: order.command,
            sugar: Math.max(0, Math.min(2, order.sugar ?? 0)),
            stick: order.stick != null ? order.stick : order.sugar > 0,
        });
    }
    
}

