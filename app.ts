import input from 'input';
import moment from 'moment';

import 'reflect-metadata';
import { Container } from 'inversify';
import { instance } from 'ts-mockito';

import { CommandType } from './types/enums';
import { StateService } from './types/state';
import { ProtocolTranslatorService } from './types/coffee';
import { EmailNotifierInterface, BeverageQuantityCheckerInterface } from './types/external';

import { MockEmailNotifier } from './mock/EmailNotifier';
import { BeverageQuantityCheckerMockFactory } from './mock/BeverageQuantityChecker';

import { ProtocolTranslator } from './services/ProtocolTranslator';
import { CoffeeMachineState } from './services/CoffeeMachineState';

function today(): string {
    return moment().format('YYYY-MM-DD');
}

function help(): string {
    return [
        'Type your order',
        ' ex: T 1 0.4',
        '     Ch 0 0.6',
        '',
        'Available additional commands are:',
        ' help',
        ' report [YYYY-MM-DD]',
        ' exit|quit|q',
        '',
    ].join('\n');
}

function containerSetup(): Container {
    const container = new Container();
    container.bind<EmailNotifierInterface>('EmailNotifierInterface').toConstantValue(instance(MockEmailNotifier));
    container.bind<BeverageQuantityCheckerInterface>('BeverageQuantityCheckerInterface').toConstantValue(instance(BeverageQuantityCheckerMockFactory()));
    container.bind<StateService>('StateService').to(CoffeeMachineState).inSingletonScope();
    container.bind<ProtocolTranslatorService>('ProtocolTranslatorService').to(ProtocolTranslator);
    return container;
}

async function run(): Promise<void> {
    const container = containerSetup();
    const coffeeMachineState: CoffeeMachineState = container.get('StateService');
    const coffeeMachine: ProtocolTranslatorService = container.get('ProtocolTranslatorService');

    console.log(help());
    let shouldExit = false;
    while (!shouldExit) {
        const commandString = await input.text('order (or cmd): ');
        const args = commandString.trim().split(/\s+/).filter((s: string) => s.trim());
        switch (args[0]) {
            case 'q':
            case 'quit':
            case 'exit':
                shouldExit = true;
                break;
            case 'report':
                console.log(coffeeMachineState.report(args[1] ?? today()));
                break;
            case 'help':
                console.log(help());
                break;
            case CommandType.message:
                console.log(coffeeMachine.order({
                    command: CommandType.message,
                    message: args[1],
                }));
                break;
            default:
                console.log(coffeeMachine.order({
                    command: args[0],
                    sugar: args[1],
                    cash: args[2],
                }));
                break;
        }
    }
    console.log(coffeeMachineState.report(today()));
    console.log('\nGoodbye !');
}

run();
