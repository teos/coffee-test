import equal from 'deep-equal';
import { mock, when, anything } from 'ts-mockito';

import { CommandType } from '../types/enums';
import { ProtocolResult } from '../types/protocol';
import { ProtocolTranslatorService, OrderData } from '../types/coffee';

const orders: { input: OrderData; result: ProtocolResult; called?: number }[] = [
    { input: { command: null, cash: 1 }, result: 'M:Invalid input' },
    { input: { command: undefined, cash: 1 }, result: 'M:Invalid input' },
    { input: { command: '', cash: 1 }, result: 'M:Invalid input' },
    { input: { command: 'yolo', cash: 1 }, result: 'M:Invalid input' },
    { input: { command: 4, cash: 1 }, result: 'M:Invalid input' },
    { input: { command: CommandType.tea, sugar: null, cash: 1 }, result: 'M:Invalid input' },
    { input: { command: CommandType.tea, sugar: '', cash: 1 }, result: 'M:Invalid input' },
    { input: { command: CommandType.tea, sugar: 'yolo', cash: 1 }, result: 'M:Invalid input' },
    { input: { command: CommandType.tea, sugar: 1, cash: null }, result: 'M:Invalid input' },
    { input: { command: CommandType.tea, sugar: 1, cash: '' }, result: 'M:Invalid input' },
    { input: { command: CommandType.tea, sugar: 1, cash: 'yolo' }, result: 'M:Invalid input' },

    { input: { command: CommandType.tea }, result: 'T::' },
    { input: { command: CommandType.tea, sugar: -1, cash: 0.4 }, result: 'M:Invalid input' },
    { input: { command: CommandType.tea, sugar: 0, cash: 0.4 }, result: 'T::' },
    { input: { command: CommandType.tea, sugar: 1, cash: 0.4 }, result: 'T:1:0' },
    { input: { command: CommandType.tea, sugar: 2, cash: 0.4 }, result: 'T:2:0' },
    { input: { command: CommandType.tea, sugar: 3, cash: 0.4 }, result: 'T:2:0' },
    { input: { command: CommandType.tea, sugar: 3, cash: 10 }, result: 'T:2:0' },
    { input: { command: CommandType.hotChocolate }, result: 'H::' },
    { input: { command: CommandType.hotChocolate, sugar: -1, cash: 0.5 }, result: 'M:Invalid input' },
    { input: { command: CommandType.hotChocolate, sugar: 0, cash: 0.5 }, result: 'H::' },
    { input: { command: CommandType.hotChocolate, sugar: 1, cash: 0.5 }, result: 'H:1:0' },
    { input: { command: CommandType.hotChocolate, sugar: 2, cash: 0.5 }, result: 'H:2:0' },
    { input: { command: CommandType.hotChocolate, sugar: 3, cash: 0.5 }, result: 'H:2:0' },
    { input: { command: CommandType.hotChocolate, sugar: 3, cash: 10 }, result: 'H:2:0' },
    { input: { command: CommandType.coffee }, result: 'C::' },
    { input: { command: CommandType.coffee, sugar: -1, cash: 0.6 }, result: 'M:Invalid input' },
    { input: { command: CommandType.coffee, sugar: 0, cash: 0.6 }, result: 'C::' },
    { input: { command: CommandType.coffee, sugar: 1, cash: 0.6 }, result: 'C:1:0' },
    { input: { command: CommandType.coffee, sugar: 2, cash: 0.6 }, result: 'C:2:0' },
    { input: { command: CommandType.coffee, sugar: 3, cash: 0.6 }, result: 'C:2:0' },
    { input: { command: CommandType.coffee, sugar: 3, cash: 10 }, result: 'C:2:0' },
    { input: { command: CommandType.orangeJuice }, result: 'O::' },
    { input: { command: CommandType.orangeJuice, sugar: -1, cash: 0.6 }, result: 'M:Invalid input' },
    { input: { command: CommandType.orangeJuice, sugar: 0, cash: 0.6 }, result: 'O::' },
    { input: { command: CommandType.orangeJuice, sugar: 1, cash: 0.6 }, result: 'O:1:0' },
    { input: { command: CommandType.orangeJuice, sugar: 2, cash: 0.6 }, result: 'O:2:0' },
    { input: { command: CommandType.orangeJuice, sugar: 3, cash: 0.6 }, result: 'O:2:0' },
    { input: { command: CommandType.orangeJuice, sugar: 3, cash: 10 }, result: 'O:2:0' },
    { input: { command: CommandType.extraHotTea }, result: 'Th::' },
    { input: { command: CommandType.extraHotTea, sugar: -1, cash: 0.4 }, result: 'M:Invalid input' },
    { input: { command: CommandType.extraHotTea, sugar: 0, cash: 0.4 }, result: 'Th::' },
    { input: { command: CommandType.extraHotTea, sugar: 1, cash: 0.4 }, result: 'Th:1:0' },
    { input: { command: CommandType.extraHotTea, sugar: 2, cash: 0.4 }, result: 'Th:2:0' },
    { input: { command: CommandType.extraHotTea, sugar: 3, cash: 0.4 }, result: 'Th:2:0' },
    { input: { command: CommandType.extraHotTea, sugar: 3, cash: 10 }, result: 'Th:2:0' },
    { input: { command: CommandType.extraHotChocolate }, result: 'Hh::' },
    { input: { command: CommandType.extraHotChocolate, sugar: -1, cash: 0.5 }, result: 'M:Invalid input' },
    { input: { command: CommandType.extraHotChocolate, sugar: 0, cash: 0.5 }, result: 'Hh::' },
    { input: { command: CommandType.extraHotChocolate, sugar: 1, cash: 0.5 }, result: 'Hh:1:0' },
    { input: { command: CommandType.extraHotChocolate, sugar: 2, cash: 0.5 }, result: 'Hh:2:0' },
    { input: { command: CommandType.extraHotChocolate, sugar: 3, cash: 0.5 }, result: 'Hh:2:0' },
    { input: { command: CommandType.extraHotChocolate, sugar: 3, cash: 10 }, result: 'Hh:2:0' },
    { input: { command: CommandType.extraHotCoffee }, result: 'Ch::' },
    { input: { command: CommandType.extraHotCoffee, sugar: -1, cash: 0.6 }, result: 'M:Invalid input' },
    { input: { command: CommandType.extraHotCoffee, sugar: 0, cash: 0.6 }, result: 'Ch::' },
    { input: { command: CommandType.extraHotCoffee, sugar: 1, cash: 0.6 }, result: 'Ch:1:0' },
    { input: { command: CommandType.extraHotCoffee, sugar: 2, cash: 0.6 }, result: 'Ch:2:0' },
    { input: { command: CommandType.extraHotCoffee, sugar: 3, cash: 0.6 }, result: 'Ch:2:0' },
    { input: { command: CommandType.extraHotCoffee, sugar: 3, cash: 10 }, result: 'Ch:2:0' },

    { input: { command: CommandType.message }, result: 'M:' },
    { input: { command: CommandType.message, message: '' }, result: 'M:' },
    { input: { command: CommandType.message, message: 'test' }, result: 'M:test' },
    { input: { command: CommandType.message, message: '1234567890' }, result: 'M:1234567890' },

    { input: { command: CommandType.tea, sugar: 3, cash: -1 }, result: 'M:Invalid input' },
    { input: { command: CommandType.tea, sugar: 3, cash: 0 }, result: 'M:Missing 0.4 euros' },
    { input: { command: CommandType.tea, sugar: 1, cash: 0.3 }, result: 'M:Missing 0.1 euros' },
    { input: { command: CommandType.tea, sugar: 1, cash: 0.1 }, result: 'M:Missing 0.3 euros' },
    { input: { command: CommandType.tea, sugar: 1, cash: -0.6 }, result: 'M:Invalid input' },
    { input: { command: CommandType.hotChocolate, sugar: 3, cash: -1 }, result: 'M:Invalid input' },
    { input: { command: CommandType.hotChocolate, sugar: 3, cash: 0 }, result: 'M:Missing 0.5 euros' },
    { input: { command: CommandType.hotChocolate, sugar: 1, cash: 0.4 }, result: 'M:Missing 0.1 euros' },
    { input: { command: CommandType.hotChocolate, sugar: 1, cash: 0.2 }, result: 'M:Missing 0.3 euros' },
    { input: { command: CommandType.hotChocolate, sugar: 1, cash: -0.5 }, result: 'M:Invalid input' },
    { input: { command: CommandType.coffee, sugar: 3, cash: -1 }, result: 'M:Invalid input' },
    { input: { command: CommandType.coffee, sugar: 3, cash: 0 }, result: 'M:Missing 0.6 euros' },
    { input: { command: CommandType.coffee, sugar: 1, cash: 0.5 }, result: 'M:Missing 0.1 euros' },
    { input: { command: CommandType.coffee, sugar: 1, cash: 0.3 }, result: 'M:Missing 0.3 euros' },
    { input: { command: CommandType.coffee, sugar: 1, cash: -0.4 }, result: 'M:Invalid input' },

    { input: { command: CommandType.tea, cash: 1 }, result: 'T::' },
];

export const MockProtocolTranslator: ProtocolTranslatorService = mock<ProtocolTranslatorService>();
when(MockProtocolTranslator.order(anything())).thenCall((data: OrderData) => {
    const orderData = orders.find(o => equal(o.input, data));
    if (orderData) {
        orderData.called = (orderData.called ?? 0) + 1;
        if (orderData.called > 5) {
            return 'M:Shortage, the service company has been notified';
        }
    }
    return orderData?.result; 
});
when(MockProtocolTranslator.checkCash(CommandType.tea, -1)).thenReturn(0.4);
when(MockProtocolTranslator.checkCash(CommandType.tea, 0)).thenReturn(0.4);
when(MockProtocolTranslator.checkCash(CommandType.tea, 0.3)).thenReturn(0.1);
when(MockProtocolTranslator.checkCash(CommandType.tea, 0.1)).thenReturn(0.3);
when(MockProtocolTranslator.checkCash(CommandType.tea, 0.4)).thenReturn(0);
when(MockProtocolTranslator.checkCash(CommandType.tea, 1)).thenReturn(-0.6);
when(MockProtocolTranslator.checkCash(CommandType.hotChocolate, -1)).thenReturn(0.5);
when(MockProtocolTranslator.checkCash(CommandType.hotChocolate, 0)).thenReturn(0.5);
when(MockProtocolTranslator.checkCash(CommandType.hotChocolate, 0.4)).thenReturn(0.1);
when(MockProtocolTranslator.checkCash(CommandType.hotChocolate, 0.2)).thenReturn(0.3);
when(MockProtocolTranslator.checkCash(CommandType.hotChocolate, 0.5)).thenReturn(0);
when(MockProtocolTranslator.checkCash(CommandType.hotChocolate, 1)).thenReturn(-0.5);
when(MockProtocolTranslator.checkCash(CommandType.coffee, -1)).thenReturn(0.6);
when(MockProtocolTranslator.checkCash(CommandType.coffee, 0)).thenReturn(0.6);
when(MockProtocolTranslator.checkCash(CommandType.coffee, 0.5)).thenReturn(0.1);
when(MockProtocolTranslator.checkCash(CommandType.coffee, 0.3)).thenReturn(0.3);
when(MockProtocolTranslator.checkCash(CommandType.coffee, 0.6)).thenReturn(0);
when(MockProtocolTranslator.checkCash(CommandType.coffee, 1)).thenReturn(-0.4);
when(MockProtocolTranslator.checkCash(CommandType.orangeJuice, -1)).thenReturn(0.6);
when(MockProtocolTranslator.checkCash(CommandType.orangeJuice, 0)).thenReturn(0.6);
when(MockProtocolTranslator.checkCash(CommandType.orangeJuice, 0.5)).thenReturn(0.1);
when(MockProtocolTranslator.checkCash(CommandType.orangeJuice, 0.3)).thenReturn(0.3);
when(MockProtocolTranslator.checkCash(CommandType.orangeJuice, 0.6)).thenReturn(0);
when(MockProtocolTranslator.checkCash(CommandType.orangeJuice, 1)).thenReturn(-0.4);
when(MockProtocolTranslator.checkCash(CommandType.extraHotTea, -1)).thenReturn(0.4);
when(MockProtocolTranslator.checkCash(CommandType.extraHotTea, 0)).thenReturn(0.4);
when(MockProtocolTranslator.checkCash(CommandType.extraHotTea, 0.3)).thenReturn(0.1);
when(MockProtocolTranslator.checkCash(CommandType.extraHotTea, 0.1)).thenReturn(0.3);
when(MockProtocolTranslator.checkCash(CommandType.extraHotTea, 0.4)).thenReturn(0);
when(MockProtocolTranslator.checkCash(CommandType.extraHotTea, 1)).thenReturn(-0.6);
when(MockProtocolTranslator.checkCash(CommandType.extraHotChocolate, -1)).thenReturn(0.5);
when(MockProtocolTranslator.checkCash(CommandType.extraHotChocolate, 0)).thenReturn(0.5);
when(MockProtocolTranslator.checkCash(CommandType.extraHotChocolate, 0.4)).thenReturn(0.1);
when(MockProtocolTranslator.checkCash(CommandType.extraHotChocolate, 0.2)).thenReturn(0.3);
when(MockProtocolTranslator.checkCash(CommandType.extraHotChocolate, 0.5)).thenReturn(0);
when(MockProtocolTranslator.checkCash(CommandType.extraHotChocolate, 1)).thenReturn(-0.5);
when(MockProtocolTranslator.checkCash(CommandType.extraHotCoffee, -1)).thenReturn(0.6);
when(MockProtocolTranslator.checkCash(CommandType.extraHotCoffee, 0)).thenReturn(0.6);
when(MockProtocolTranslator.checkCash(CommandType.extraHotCoffee, 0.5)).thenReturn(0.1);
when(MockProtocolTranslator.checkCash(CommandType.extraHotCoffee, 0.3)).thenReturn(0.3);
when(MockProtocolTranslator.checkCash(CommandType.extraHotCoffee, 0.6)).thenReturn(0);
when(MockProtocolTranslator.checkCash(CommandType.extraHotCoffee, 1)).thenReturn(-0.4);

export default MockProtocolTranslator;
