import { mock, when, anyString } from 'ts-mockito';

import { EmailNotifierInterface } from '../types/external';

export const MockEmailNotifier: EmailNotifierInterface = mock<EmailNotifierInterface>();
when(MockEmailNotifier.notifyMissingDrink(anyString())).thenReturn();
