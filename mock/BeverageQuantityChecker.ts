import { mock, when } from 'ts-mockito';

import { CommandType } from '../types/enums';
import { BeverageQuantityCheckerInterface } from '../types/external';

export function BeverageQuantityCheckerMockFactory(): BeverageQuantityCheckerInterface {
    const MockBeverageQuantityChecker = mock<BeverageQuantityCheckerInterface>();
    when(MockBeverageQuantityChecker.isEmpty(CommandType.hotChocolate)).thenReturn(true);
    when(MockBeverageQuantityChecker.isEmpty(CommandType.coffee)).thenReturn(true);
    when(MockBeverageQuantityChecker.isEmpty(CommandType.orangeJuice)).thenReturn(true);
    when(MockBeverageQuantityChecker.isEmpty(CommandType.extraHotTea)).thenReturn(true);
    when(MockBeverageQuantityChecker.isEmpty(CommandType.extraHotChocolate)).thenReturn(true);
    when(MockBeverageQuantityChecker.isEmpty(CommandType.extraHotCoffee)).thenReturn(true);
    when(MockBeverageQuantityChecker.isEmpty(CommandType.tea))
        .thenReturn(true)
        .thenReturn(true)
        .thenReturn(true)
        .thenReturn(true)
        .thenReturn(true)
        .thenReturn(false);
    return MockBeverageQuantityChecker;
}
