import moment from 'moment';
import { mock, when, anyString, anyNumber } from 'ts-mockito';

import { StateService } from '../types/state';

const yesterday = moment().subtract(1, 'day').format('YYYY-MM-DD');
const today = moment().format('YYYY-MM-DD');
const tomorrow = moment().add(1, 'day').format('YYYY-MM-DD');

export const MockStateService: StateService = mock<StateService>();
when(MockStateService.sold(anyString(), anyNumber())).thenReturn();
when(MockStateService.report('')).thenThrow(new Error('Error: Invalid date ""'));
when(MockStateService.report('yolo')).thenThrow(new Error('Error: Invalid date "yolo"'));
when(MockStateService.report(yesterday)).thenReturn([
    ' === Quantities === ',
    '  Tea: 0',
    '  Hot chocolate: 0',
    '  Coffee: 0',
    '  Orange juice: 0',
    '  Extra Hot Tea: 0',
    '  Extra Hot chocolate: 0',
    '  Extra Hot Coffee: 0',
    ' === Earnings === ',
    '  total: 0.00 euros',
].join('\n'));
when(MockStateService.report(tomorrow)).thenReturn([
    ' === Quantities === ',
    '  Tea: 0',
    '  Hot chocolate: 0',
    '  Coffee: 0',
    '  Orange juice: 0',
    '  Extra Hot Tea: 0',
    '  Extra Hot chocolate: 0',
    '  Extra Hot Coffee: 0',
    ' === Earnings === ',
    '  total: 0.00 euros',
].join('\n'));
when(MockStateService.report(today)).thenReturn([
    ' === Quantities === ',
    '  Tea: 0',
    '  Hot chocolate: 0',
    '  Coffee: 0',
    '  Orange juice: 0',
    '  Extra Hot Tea: 0',
    '  Extra Hot chocolate: 0',
    '  Extra Hot Coffee: 0',
    ' === Earnings === ',
    '  total: 0.00 euros',
].join('\n')).thenReturn([
    ' === Quantities === ',
    '  Tea: 1',
    '  Hot chocolate: 2',
    '  Coffee: 3',
    '  Orange juice: 4',
    '  Extra Hot Tea: 1',
    '  Extra Hot chocolate: 2',
    '  Extra Hot Coffee: 3',
    ' === Earnings === ',
    '  total: 8.80 euros',
].join('\n')).thenReturn([
    ' === Quantities === ',
    '  Tea: 0',
    '  Hot chocolate: 2',
    '  Coffee: 3',
    '  Orange juice: 1',
    '  Extra Hot Tea: 1',
    '  Extra Hot chocolate: 0',
    '  Extra Hot Coffee: 1',
    ' === Earnings === ',
    '  total: 4.40 euros',
].join('\n'));

export default MockStateService;
