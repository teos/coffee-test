import { preciseRound } from '../helpers/math';
import { expect, containerSetup } from './setup';

import { CommandType } from '../types/enums';
import { ProtocolTranslatorService } from '../types/coffee';


function allErrorPossibilities(
    command: Exclude<CommandType, CommandType.message>,
    price: number,
): {
    command: Exclude<CommandType, CommandType.message>;
    cash: number;
    result: number;
}[] {
    return [
        { command, cash: -1, result: price },
        { command, cash: 0, result: price },
        { command, cash: price, result: 0 },
        { command, cash: preciseRound(price - 0.1, 2), result: 0.1 },
        { command, cash: preciseRound(price - 0.3, 2), result: 0.3 },
        { command, cash: 1, result: preciseRound(price - 1, 2) },
    ];
}

describe('ProtocolTranslatorService#order', () => {
    let protocol: ProtocolTranslatorService;

    beforeEach(() => {
        const container = containerSetup();
        protocol = container.get('ProtocolTranslatorService');
    });

    it('should return the difference in cash', () => {
        for (const order of allErrorPossibilities(CommandType.tea, 0.4)) {
            expect(protocol.checkCash(order.command, order.cash)).to.equal(order.result);
        }
        for (const order of allErrorPossibilities(CommandType.hotChocolate, 0.5)) {
            expect(protocol.checkCash(order.command, order.cash)).to.equal(order.result);
        }
        for (const order of allErrorPossibilities(CommandType.coffee, 0.6)) {
            expect(protocol.checkCash(order.command, order.cash)).to.equal(order.result);
        }
        for (const order of allErrorPossibilities(CommandType.orangeJuice, 0.6)) {
            expect(protocol.checkCash(order.command, order.cash)).to.equal(order.result);
        }
        for (const order of allErrorPossibilities(CommandType.extraHotTea, 0.4)) {
            expect(protocol.checkCash(order.command, order.cash)).to.equal(order.result);
        }
        for (const order of allErrorPossibilities(CommandType.extraHotChocolate, 0.5)) {
            expect(protocol.checkCash(order.command, order.cash)).to.equal(order.result);
        }
        for (const order of allErrorPossibilities(CommandType.extraHotCoffee, 0.6)) {
            expect(protocol.checkCash(order.command, order.cash)).to.equal(order.result);
        }
    });

});
