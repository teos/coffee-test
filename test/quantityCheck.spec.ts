import { expect, containerSetup } from './setup';

import { CommandType } from '../types/enums';
import { ProtocolTranslatorService } from '../types/coffee';


describe('BeverageQuantityChecker#order', () => {
    let protocol: ProtocolTranslatorService;

    beforeEach(() => {
        const container = containerSetup();
        protocol = container.get('ProtocolTranslatorService');
    });

    it('should not allow to buy more than 5 times', () => {
        expect(protocol.order({ command: CommandType.tea, cash: 1 })).to.equal('T::');
        expect(protocol.order({ command: CommandType.tea, cash: 1 })).to.equal('T::');
        expect(protocol.order({ command: CommandType.tea, cash: 1 })).to.equal('T::');
        expect(protocol.order({ command: CommandType.tea, cash: 1 })).to.equal('T::');
        expect(protocol.order({ command: CommandType.tea, cash: 1 })).to.equal('T::');
        expect(protocol.order({ command: CommandType.tea, cash: 1 })).to.equal('M:Shortage, the service company has been notified');
        expect(protocol.order({ command: CommandType.tea, cash: 1 })).to.equal('M:Shortage, the service company has been notified');
        expect(protocol.order({ command: CommandType.tea, cash: 1 })).to.equal('M:Shortage, the service company has been notified');
    });

});
