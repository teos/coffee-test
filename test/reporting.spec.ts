import moment from 'moment';

import { expect, containerSetup } from './setup';

import { CommandType } from '../types/enums';
import { StateService } from '../types/state';
import { ProtocolTranslatorService } from '../types/coffee';

const yesterday = moment().subtract(1, 'day').format('YYYY-MM-DD');
const today = moment().format('YYYY-MM-DD');
const tomorrow = moment().add(1, 'day').format('YYYY-MM-DD');

describe('StateService#report', () => {
    let state: StateService;
    let protocol: ProtocolTranslatorService;

    function orderQuantity(command: CommandType, quantity: number): void {
        for (let i=0; i<quantity; i++) {
            protocol.order({ command, cash: 1 });
        }
    }

    beforeEach(() => {
        const container = containerSetup();
        state = container.get('StateService');
        protocol = container.get('ProtocolTranslatorService');
    });

    it('should print an error', () => {
        expect(() => state.report('')).to.throw(Error, 'Error: Invalid date ""');
        expect(() => state.report('yolo')).to.throw(Error, 'Error: Invalid date "yolo"');
    });

    it('should print an empty report', () => {
        expect(state.report(today)).to.equal([
            ' === Quantities === ',
            '  Tea: 0',
            '  Hot chocolate: 0',
            '  Coffee: 0',
            '  Orange juice: 0',
            '  Extra Hot Tea: 0',
            '  Extra Hot chocolate: 0',
            '  Extra Hot Coffee: 0',
            ' === Earnings === ',
            '  total: 0.00 euros',
        ].join('\n'));
    });

    it('should print an empty report for yesterday', () => {
        orderQuantity(CommandType.tea, 1);
        expect(state.report(yesterday)).to.equal([
            ' === Quantities === ',
            '  Tea: 0',
            '  Hot chocolate: 0',
            '  Coffee: 0',
            '  Orange juice: 0',
            '  Extra Hot Tea: 0',
            '  Extra Hot chocolate: 0',
            '  Extra Hot Coffee: 0',
            ' === Earnings === ',
            '  total: 0.00 euros',
        ].join('\n'));
    });

    it('should print an empty report for tomorrow', () => {
        orderQuantity(CommandType.tea, 1);
        expect(state.report(tomorrow)).to.equal([
            ' === Quantities === ',
            '  Tea: 0',
            '  Hot chocolate: 0',
            '  Coffee: 0',
            '  Orange juice: 0',
            '  Extra Hot Tea: 0',
            '  Extra Hot chocolate: 0',
            '  Extra Hot Coffee: 0',
            ' === Earnings === ',
            '  total: 0.00 euros',
        ].join('\n'));
    });

    it('should print a report', () => {
        orderQuantity(CommandType.tea, 1);
        orderQuantity(CommandType.hotChocolate, 2);
        orderQuantity(CommandType.coffee, 3);
        orderQuantity(CommandType.orangeJuice, 4);
        orderQuantity(CommandType.extraHotTea, 1);
        orderQuantity(CommandType.extraHotChocolate, 2);
        orderQuantity(CommandType.extraHotCoffee, 3);
        expect(state.report(today)).to.equal([
            ' === Quantities === ',
            '  Tea: 1',
            '  Hot chocolate: 2',
            '  Coffee: 3',
            '  Orange juice: 4',
            '  Extra Hot Tea: 1',
            '  Extra Hot chocolate: 2',
            '  Extra Hot Coffee: 3',
            ' === Earnings === ',
            '  total: 8.80 euros',
        ].join('\n'));
    });

    it('should print a report (case 2)', () => {
        orderQuantity(CommandType.tea, 0);
        orderQuantity(CommandType.hotChocolate, 2);
        orderQuantity(CommandType.coffee, 3);
        orderQuantity(CommandType.orangeJuice, 1);
        orderQuantity(CommandType.extraHotTea, 1);
        orderQuantity(CommandType.extraHotChocolate, 0);
        orderQuantity(CommandType.extraHotCoffee, 1);
        expect(state.report(today)).to.equal([
            ' === Quantities === ',
            '  Tea: 0',
            '  Hot chocolate: 2',
            '  Coffee: 3',
            '  Orange juice: 1',
            '  Extra Hot Tea: 1',
            '  Extra Hot chocolate: 0',
            '  Extra Hot Coffee: 1',
            ' === Earnings === ',
            '  total: 4.40 euros',
        ].join('\n'));
    });

});
