import { preciseRound } from '../helpers/math';
import { expect, containerSetup } from './setup';

import { CommandType } from '../types/enums';
import { ProtocolResult } from '../types/protocol';
import { ProtocolTranslatorService, OrderData } from '../types/coffee';

function allOrderPossibilities(
    command: Exclude<CommandType, CommandType.message>,
    typeValue: string,
    price: number,
): {
    input: OrderData;
    result: ProtocolResult;
}[] {
    return [
        { input: { command, sugar: -1, cash: price }, result: 'M:Invalid input' },
        { input: { command, sugar: 0, cash: price }, result: `${typeValue}::` },
        { input: { command, sugar: 1, cash: price }, result: `${typeValue}:1:0` },
        { input: { command, sugar: 2, cash: price }, result: `${typeValue}:2:0` },
        { input: { command, sugar: 3, cash: price }, result: `${typeValue}:2:0` },
        { input: { command, sugar: 3, cash: 10 }, result: `${typeValue}:2:0` },
    ];
}

function allErrorPossibilities(
    command: Exclude<CommandType, CommandType.message>,
    price: number,
): {
    input: OrderData;
    result: ProtocolResult;
}[] {
    return [
        { input: { command, sugar: 3, cash: -1 }, result: 'M:Invalid input' },
        { input: { command, sugar: 3, cash: 0 }, result: `M:Missing ${price} euros` },
        { input: { command, sugar: 1, cash: preciseRound(price - 0.1, 2) }, result: 'M:Missing 0.1 euros' },
        { input: { command, sugar: 1, cash: preciseRound(price - 0.3, 2) }, result: 'M:Missing 0.3 euros' },
        { input: { command, sugar: 1, cash: preciseRound(price - 1, 2) }, result: 'M:Invalid input' },
    ];
}

describe('ProtocolTranslatorService#order', () => {
    let protocol: ProtocolTranslatorService;

    beforeEach(() => {
        const container = containerSetup();
        protocol = container.get('ProtocolTranslatorService');
    });

    it('validate input data', () => {
        expect(protocol.order({ command: null, cash: 1 })).to.equal('M:Invalid input');
        expect(protocol.order({ command: undefined, cash: 1 })).to.equal('M:Invalid input');
        expect(protocol.order({ command: '', cash: 1 })).to.equal('M:Invalid input');
        expect(protocol.order({ command: 'yolo', cash: 1 })).to.equal('M:Invalid input');
        expect(protocol.order({ command: 4, cash: 1 })).to.equal('M:Invalid input');
        expect(protocol.order({ command: CommandType.tea, sugar: null, cash: 1 })).to.equal('M:Invalid input');
        expect(protocol.order({ command: CommandType.tea, sugar: '', cash: 1 })).to.equal('M:Invalid input');
        expect(protocol.order({ command: CommandType.tea, sugar: 'yolo', cash: 1 })).to.equal('M:Invalid input');
        expect(protocol.order({ command: CommandType.tea, sugar: 1, cash: null })).to.equal('M:Invalid input');
        expect(protocol.order({ command: CommandType.tea, sugar: 1, cash: '' })).to.equal('M:Invalid input');
        expect(protocol.order({ command: CommandType.tea, sugar: 1, cash: 'yolo' })).to.equal('M:Invalid input');
    });

    it('should allow to buy tea', () => {
        for (const order of allOrderPossibilities(CommandType.tea, 'T', 0.4)) {
            expect(protocol.order(order.input)).to.equal(order.result);
        }
    });

    it('should allow to buy hot chocolate', () => {
        for (const order of allOrderPossibilities(CommandType.hotChocolate, 'H', 0.5)) {
            expect(protocol.order(order.input)).to.equal(order.result);
        }
    });

    it('should allow to buy coffee', () => {
        for (const order of allOrderPossibilities(CommandType.coffee, 'C', 0.6)) {
            expect(protocol.order(order.input)).to.equal(order.result);
        }
    });

    it('should not allow to buy without enough cash', () => {
        for (const order of allErrorPossibilities(CommandType.tea, 0.4)) {
            expect(protocol.order(order.input)).to.equal(order.result);
        }
        for (const order of allErrorPossibilities(CommandType.hotChocolate, 0.5)) {
            expect(protocol.order(order.input)).to.equal(order.result);
        }
        for (const order of allErrorPossibilities(CommandType.coffee, 0.6)) {
            expect(protocol.order(order.input)).to.equal(order.result);
        }
    });

    it('should allow to forward a message', () => {
        const orders: { input: OrderData; result: ProtocolResult }[] = [
            { input: { command: CommandType.message }, result: 'M:' },
            { input: { command: CommandType.message, message: '' }, result: 'M:' },
            { input: { command: CommandType.message, message: 'test' }, result: 'M:test' },
            { input: { command: CommandType.message, message: '1234567890' }, result: 'M:1234567890' },
        ];
        for (const order of orders) {
            expect(protocol.order(order.input)).to.equal(order.result);
        }
    });

});
