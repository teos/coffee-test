import chai from 'chai';

import 'reflect-metadata';
import { Container } from 'inversify';
import { instance } from 'ts-mockito';

import { StateService } from '../types/state';
import { ProtocolTranslatorService } from '../types/coffee';
import { EmailNotifierInterface, BeverageQuantityCheckerInterface } from '../types/external';

import { MockEmailNotifier } from '../mock/EmailNotifier';
import { BeverageQuantityCheckerMockFactory } from '../mock/BeverageQuantityChecker';
// import { MockProtocolTranslator } from '../mock/ProtocolTranslator';
// import { MockStateService } from '../mock/StateService';
import { ProtocolTranslator } from '../services/ProtocolTranslator';
import { CoffeeMachineState } from '../services/CoffeeMachineState';

export const expect = chai.expect;

export function containerSetup(): Container {
    const container = new Container();
    container.bind<EmailNotifierInterface>('EmailNotifierInterface').toConstantValue(instance(MockEmailNotifier));
    container.bind<BeverageQuantityCheckerInterface>('BeverageQuantityCheckerInterface').toConstantValue(instance(BeverageQuantityCheckerMockFactory()));
    // container.bind<StateService>('StateService').toConstantValue(instance(MockStateService));
    // container.bind<ProtocolTranslatorService>('ProtocolTranslatorService').toConstantValue(instance(MockProtocolTranslator));
    container.bind<StateService>('StateService').to(CoffeeMachineState).inSingletonScope();
    container.bind<ProtocolTranslatorService>('ProtocolTranslatorService').to(ProtocolTranslator);
    return container;
}
