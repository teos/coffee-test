
export function preciseRound(n: number, fixed: number = 6): number {
    let mutl = 1;
    for (let i = 0; i < fixed; i++) {
        mutl *= 10;
    }
    return Math.round((n + Number.EPSILON) * mutl) / mutl;
}
