
module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    plugins: [
        '@typescript-eslint',
    ],
    extends: [
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    rules: {
        'no-console': 'off',
        'quotes': [ 'error', 'single', {
            avoidEscape: true,
        }],
        'comma-dangle': [ 'warn', 'always-multiline' ],

        // TS specific rules
        '@typescript-eslint/no-inferrable-types': 'off',
        '@typescript-eslint/no-unused-vars': [ 'warn', {
            ignoreRestSiblings: true,
            argsIgnorePattern: '^_',
        }],
        '@typescript-eslint/semi': [ 'error' ],

        // Disables ESLint rules in order to user TS specific rules
        'semi': 'off',
        'no-unused-vars': 'off',
    },
};
