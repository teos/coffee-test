
export interface EmailNotifierInterface {
	notifyMissingDrink(drink: string): void;
}

export interface BeverageQuantityCheckerInterface {
	isEmpty(drink: string): boolean;
}
