
export enum CommandType {
    message = 'M',
    tea = 'T',
    coffee = 'C',
    hotChocolate = 'H',
    orangeJuice = 'O',
    extraHotTea = 'Th',
    extraHotChocolate = 'Hh',
    extraHotCoffee = 'Ch',
}
