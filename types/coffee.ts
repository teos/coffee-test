
import { CommandType } from './enums';
import { ProtocolData, ProtocolResult } from './protocol';

export type OrderData = {
    command: unknown;
    sugar?: unknown;
    stick?: unknown;
    cash?: unknown;
} |  {
    command: Extract<CommandType, CommandType.message>;
    message?: string;
};

export interface ProtocolTranslatorService {
    translate(data: ProtocolData): ProtocolResult;
    checkCash(command: Exclude<CommandType, CommandType.message>, cash: number): number;
    order(data: OrderData): ProtocolResult;
}
