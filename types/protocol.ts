
import { CommandType } from './enums';

export type ProtocolResult = string;
export type ProtocolData = {
    command: Exclude<CommandType, CommandType.message>;
    sugar: number;
    stick: boolean;
} |  {
    command: Extract<CommandType, CommandType.message>;
    message: string;
};
