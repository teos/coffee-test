import { CommandType } from './enums';

export interface StateService {
    report(date: string): string;
    sold(type: Exclude<CommandType, CommandType.message>, price: number): void;
}